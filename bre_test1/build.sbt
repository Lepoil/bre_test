import sbt.Keys.libraryDependencies

ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.11.8"

ThisBuild/coverageEnabled := true


lazy val root = (project in file("."))
  .settings(
    name := "Test",
    libraryDependencies ++= Seq(
        "org.apache.spark" % "spark-core_2.11" % "2.3.1",
        "org.apache.spark" % "spark-sql_2.11" % "2.3.0",
        "log4j" % "log4j" % "1.2.17",
        "org.scalatest" %% "scalatest" % "3.2.15" % Test,
        "com.typesafe" % "config" % "1.4.2",
        "junit" % "junit" % "4.12",
        "com.vladsch.flexmark" % "flexmark-all" % "0.62.2"
    ),

  )


