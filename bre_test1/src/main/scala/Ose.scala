import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions.{col, from_json}
import org.apache.spark.sql.types.{DataType, StructType}

import java.net.URL
import scala.io.Source

object Ose extends App {

  val spark = SparkSession.builder()
    .appName("ose")
    .master("local[*]")
    .getOrCreate()

  val oseDF = spark.read.options(
    Map(
      "header"->"true",
      "inferSchema"->"true",
      "delimiter"->";"
    )
  ).csv("src/main/resources/ose.csv")
  oseDF.show(false)
  oseDF.printSchema()

  val jsonSchema = getJsonSchemaFromFile("ose.json")

  val oseParsedDF = parseStringColToJson(oseDF, "data", jsonSchema)
  oseParsedDF.show(false)
  oseParsedDF.select("data_json.orderId").show()
  oseParsedDF.printSchema()


  def getColJsonSchema(df: DataFrame, colName: String): StructType ={
    val spk = SparkSession.builder().getOrCreate()
    import spk.implicits._
    spk.read.json(df.select(col(colName)).as[String]).schema
  }

  def parseStringColToJson(df: DataFrame, colName: String, schema: DataType, replace: Boolean = false): DataFrame ={
    val name = if (replace) colName else s"${colName}_json"
    val newDF = df.withColumn(name, from_json(col(colName), schema))
    if( newDF.select(colName).head(1).isEmpty ){
      //warning empty df post parsing -> check if normal
    }
    newDF
  }

  def getJsonSchemaFromFile(path: String): StructType ={
    val resource: URL  = getClass.getResource(s"$path")
    val jsonString = Source.fromURL(resource).mkString
    val spk = SparkSession.builder().getOrCreate()
    import spk.implicits._
    val df = Seq(jsonString).toDF("json")
    getColJsonSchema(df, "json")
  }

  def getDataFrameFromCSV(path: String, delimiter: String, spark: SparkSession): DataFrame = {
    spark.read.options(
      Map(
        "header" -> "true",
        "delimiter" -> delimiter,
        "inferSchema" -> "true",
        "dateFormat" -> "yyyy-MM-dd",
        "timestampFormat" -> "yyyy-MM-dd HH:mm:ss.SSS"
      )
    ).format("csv").load("src/main/resources/" + path)
  }

}
