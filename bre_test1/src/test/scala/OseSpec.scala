
import com.esotericsoftware.minlog.Log.Logger
import org.scalatest.funspec.AnyFunSpec
import org.slf4j.LoggerFactory

class OseSpec extends AnyFunSpec{

  val log = LoggerFactory.getLogger(getClass.getName)

  describe("A list"){
    it("should have size 0") {
      assert(List.empty.size == 0)
    }

    it("should throw an IndexOutOfBoundsException when to access an element") {
      val emptyList = List();
      assertThrows[IndexOutOfBoundsException] {
        emptyList(1)
      }
    }
  }
}
