import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions.{col, from_json}
import org.apache.spark.sql.types.{DataType, StructType}

import java.net.URL
import scala.io.Source

object Hello extends App {

  val spark = SparkSession.builder()
    .appName("ose")
    .master("local[*]")
    .getOrCreate()

  import spark.implicits._

  val data = Seq(
    (1, "Toto", "Tata"),
    (2, "Tête", "Malade")
  ).toDF("id", "name", "First name")

  data.show()


  def getColJsonSchema(df: DataFrame, colName: String): StructType ={
    val spk = SparkSession.builder().getOrCreate()
    import spk.implicits._
    spk.read.json(df.select(col(colName)).as[String]).schema
  }

  def parseStringColToJson(df: DataFrame, colName: String, schema: DataType, replace: Boolean = false): DataFrame ={
    val name = if (replace) colName else s"${colName}_json"
    val newDF = df.withColumn(name, from_json(col(colName), schema))
    if( newDF.select(colName).head(1).isEmpty ){
      //warning empty df post parsing -> check if normal
    }
    newDF
  }

  def getJsonSchemaFromFile(path: String): StructType ={
    val resource: URL  = getClass.getResource(s"$path")
    val jsonString = Source.fromURL(resource).mkString
    val spk = SparkSession.builder().getOrCreate()
    import spk.implicits._
    val df = Seq(jsonString).toDF("json")
    getColJsonSchema(df, "json")
  }

  def getDataFrameFromCSV(path: String, delimiter: String, spark: SparkSession): DataFrame = {
    spark.read.options(
      Map(
        "header" -> "true",
        "delimiter" -> delimiter,
        "inferSchema" -> "true",
        "dateFormat" -> "yyyy-MM-dd",
        "timestampFormat" -> "yyyy-MM-dd HH:mm:ss.SSS"
      )
    ).format("csv").load("src/main/resources/" + path)
  }

}
