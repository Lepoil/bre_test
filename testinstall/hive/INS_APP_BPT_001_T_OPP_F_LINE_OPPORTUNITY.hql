-- --------------------------------------------------------------------------------------------
-- Creation DDL (tables and views)
-- [GIT_VERSION]
-- Table des BPT

-- --------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS ${hiveconf:app_prefix}APP_BPT_VM.T_OPP_F_LINE_OPPORTUNITY;


CREATE EXTERNAL TABLE IF NOT EXISTS ${hiveconf:app_prefix}APP_BPT_VM.T_OPP_F_LINE_OPPORTUNITY (
ID_LIGNE_OPPORTUNITE_CL	string	COMMENT 'identifiant technique ligne opportunite'
,ID_OPPORTUNITE_PUBLIC	string	COMMENT 'Numero affaire'
,CUID_VENDEUR_LIGNE	string	COMMENT 'CUID du Vendeur de la ligne opportunite'
,FONCTION_VENDEUR_LIGNE	string	COMMENT 'Libelle de la fonction du Vendeur de la ligne'
,CUID_MANAGEUR_LIGNE	string	COMMENT 'CUID du Manager du Vendeur'
,FONCTION_MANAGEUR_LIGNE	string	COMMENT 'Libelle de la fonction du Manager du Vendeur'
,DATE_SIGN_LIGNE_OPPORTUNITE	date	COMMENT 'Date de placement signature de opportunite'
,DATE_MODIF_LIGNE_OPPORTUNITE	timestamp	COMMENT 'Date de mise à jour de la ligne opportunite'
,SMB_BUSINESS_LINE_STATUS_EFORCE__C   string COMMENT 'Statut de la ligne de produit'
,BATCH_NM     string        COMMENT '@string@user ayant soumis l alimentation' 
,CREATION_TS timestamp     COMMENT '@timestamp@timestamp d insertion'
,CURRENT_IN   int           COMMENT '@int@etat courant de la cle'
,FRESH_TS  timestamp     COMMENT '@timestamp@date de fraicheur du fichier source'
,FILE_CD  string        COMMENT '@string@nom du fichier source' 

)

COMMENT 'Table des opportunites '  
PARTITIONED BY (  
               ANNEE                   string        COMMENT 'cle de partionnement',  
               MOIS                    string        COMMENT 'cle de partionnement',  
               DT                      string        COMMENT 'cle de partionnement'				
               )  
STORED AS ORC  
;  


-- View   
DROP VIEW IF EXISTS ${hiveconf:app_prefix}APP_BPT_VM_V.V_OPP_F_LINE_OPPORTUNITY;  
CREATE VIEW IF NOT EXISTS ${hiveconf:app_prefix}APP_BPT_VM_V.V_OPP_F_LINE_OPPORTUNITY
COMMENT 'View :BPT'  
AS SELECT  
*  
FROM  
${hiveconf:app_prefix}APP_BPT_VM.T_OPP_F_LINE_OPPORTUNITY
; 

