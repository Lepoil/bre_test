-- --------------------------------------------------------------------------------------------
-- Creation DDL (tables and views)
-- [GIT_VERSION]
-- Table Ligne Opportunite Negoce

-- --------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS ${hiveconf:app_prefix}APP_BPT_VM.T_OPP_F_LINE_OPPORTUNITY_NEG;


CREATE EXTERNAL TABLE IF NOT EXISTS ${hiveconf:app_prefix}APP_BPT_VM.T_OPP_F_LINE_OPPORTUNITY_NEG (

ID_LIGNE_OPPORTUNITE_CL   string COMMENT 'Ligne opportunite (ligne affaire) : identifiant technique ligne opportunite / numero de ligne'
,IDENTIFIANT_PRODUIT   string COMMENT 'code PS du produit'
,LIBELLE_PRODUIT   string COMMENT 'Libelle du produit'     
,IDENTIFIANT_COMPOSANT   string COMMENT 'code C du composant'
,LIBELLE_COMPOSANT   string COMMENT 'libellé du composant'
,QUANTITE_PRODUIT   int COMMENT 'nombre du produit'
,PCO_PCR   int COMMENT 'Produit a prix coutant ou Produit a coût réel'     
,DATE_SIGN_LIGNE_OPPORTUNITE   timestamp COMMENT 'Date de placement (signature) de lopportunite : date signature ligne opportunite (facultatif)'
,LIBELLE_CONTEXTE   string COMMENT 'contexte de la vente : fidelisation, conquête?'
,DUREE_ENGAGEMENT   int COMMENT 'durée engagement sur le produit (en mois)'
,STATUT_CL   string COMMENT 'Statut de la ligne d opportunité'
,ID_OPPORTUNITE_PUBLIC   int COMMENT 'Numéro affaire (opportunité) : identifiant public opportunité'
,DATE_CREATION_LIGNE_OPPORTUNITE   timestamp COMMENT 'date de la création de la ligne opportunité'
,DATE_MODIF_LIGNE_OPPORTUNITE   timestamp COMMENT 'date de la dernière modification sur la ligne opportunité'
,VENDEUR_LIGNE_CUID  string COMMENT 'CUID du Vendeur de la ligne d’opportunité'
,VENDEUR_LIGNE_NOM_PRENOM  string COMMENT 'Nom et prénom du vendeur de la ligne d\'opportunité'
,VENDEUR_LIGNE_FONCTION  string COMMENT 'Libellé de la fonction du vendeur de la ligne d\’opportunité'
,MANAGER_LIGNE_CUID  string COMMENT 'CUID du manager du vendeur de la ligne d’opportunité'
,MANAGER_LIGNE_NOM_PRENOM  string COMMENT 'Nom et prénom du manager du vendeur de la ligne d’opportunité' 
,MANAGER_LIGNE_FONCTION  string COMMENT 'Libellé de la fonction du manager du vendeur de la ligne d’opportunité'
,BATCH_NM     string        COMMENT '@string@user ayant soumis l alimentation' 
,CREATION_TS timestamp     COMMENT '@timestamp@timestamp d insertion'
,CURRENT_IN   int           COMMENT '@int@etat courant de la cle'
,FRESH_TS  timestamp     COMMENT '@timestamp@date de fraicheur du fichier source'
,FILE_CD  string        COMMENT '@string@nom du fichier source' 
)

COMMENT 'Table des opportunites '  
PARTITIONED BY (  
               ANNEE                   string        COMMENT 'cle de partionnement',  
               MOIS                    string        COMMENT 'cle de partionnement',  
               DT                      string        COMMENT 'cle de partionnement'				
               )  
STORED AS ORC  
;  

DROP VIEW IF EXISTS ${hiveconf:app_prefix}APP_BPT_VM.VC_OPP_F_LINE_OPPORTUNITY_NEG
;
CREATE VIEW IF NOT EXISTS ${hiveconf:app_prefix}APP_BPT_VM.VC_OPP_F_LINE_OPPORTUNITY_NEG
COMMENT 'BPT LIGNE OPPORTUNITE NEGOCE'
AS SELECT
*
FROM
${hiveconf:app_prefix}APP_BPT_VM.T_OPP_F_LINE_OPPORTUNITY_NEG
WHERE DT='1900-01-01'
;

-- View   
DROP VIEW IF EXISTS ${hiveconf:app_prefix}APP_BPT_VM_V.V_OPP_F_LINE_OPPORTUNITY_NEG;  
CREATE VIEW IF NOT EXISTS ${hiveconf:app_prefix}APP_BPT_VM_V.V_OPP_F_LINE_OPPORTUNITY_NEG
COMMENT 'View :LIGNE OPPORTUNITE NEGOCE'  
AS SELECT  
*  
FROM  
${hiveconf:app_prefix}APP_BPT_VM.T_OPP_F_LINE_OPPORTUNITY_NEG
; 

-- View_c   
DROP VIEW IF EXISTS ${hiveconf:app_prefix}APP_BPT_VM_V.V_OPP_F_LINE_OPPORTUNITY_NEG_C;  
CREATE VIEW IF NOT EXISTS ${hiveconf:app_prefix}APP_BPT_VM_V.V_OPP_F_LINE_OPPORTUNITY_NEG_C
COMMENT 'View :BPT'  
AS SELECT  
*  
FROM  
${hiveconf:app_prefix}APP_BPT_VM.VC_OPP_F_LINE_OPPORTUNITY_NEG
; 
