-- --------------------------------------------------------------------------------------------
-- Creation DDL (tables and views)
-- [GIT_VERSION]
-- Table des BPT

-- --------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS ${hiveconf:app_prefix}APP_BPT_VM.T_OPP_F_OPPORTUNITY;


CREATE EXTERNAL TABLE IF NOT EXISTS ${hiveconf:app_prefix}APP_BPT_VM.T_OPP_F_OPPORTUNITY (

ID_OPPORTUNITE_PUBLIC	string COMMENT 'Numero affaire opportunite identifiant public opportunite '
,SIREN	string COMMENT 'SIREN Entreprise'
,IDENT	string COMMENT 'IDENT Entreprise'
,SIRET	string COMMENT 'SIRET Etablissement'
,IDETA	string COMMENT 'IDETA Etablissement'
,IS_MULTI_BM	boolean COMMENT 'Tag multi-bm'
,DATE_MODIF_OPPORTUNITE	date COMMENT 'Date de mise à jour de opportunite'
,BATCH_NM     string        COMMENT '@string@user ayant soumis l alimentation' 
,CREATION_TS timestamp     COMMENT '@timestamp@timestamp d insertion'
,CURRENT_IN   int           COMMENT '@int@etat courant de la cle'
,FRESH_TS  timestamp     COMMENT '@timestamp@date de fraicheur du fichier source'
,FILE_CD  string        COMMENT '@string@nom du fichier source' 

)

COMMENT 'Table des opportunites '  
PARTITIONED BY (  
               ANNEE                   string        COMMENT 'cle de partionnement',  
               MOIS                    string        COMMENT 'cle de partionnement',  
               DT                      string        COMMENT 'cle de partionnement'				
               )  
STORED AS ORC  
;  


-- View   
DROP VIEW IF EXISTS ${hiveconf:app_prefix}APP_BPT_VM_V.V_OPP_F_OPPORTUNITY;  
CREATE VIEW IF NOT EXISTS ${hiveconf:app_prefix}APP_BPT_VM_V.V_OPP_F_OPPORTUNITY
COMMENT 'View :BPT'  
AS SELECT  
*  
FROM  
${hiveconf:app_prefix}APP_BPT_VM.T_OPP_F_OPPORTUNITY
; 
