-- --------------------------------------------------------------------------------------------
-- Creation DDL (tables and views)
-- [GIT_VERSION]
-- Table Opportunite team member Negoce

-- --------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS ${hiveconf:app_prefix}APP_BPT_VM.T_OPP_F_OPPORTUNITY_TEAM_MEMBERS_NEG;


CREATE EXTERNAL TABLE IF NOT EXISTS ${hiveconf:app_prefix}APP_BPT_VM.T_OPP_F_OPPORTUNITY_TEAM_MEMBERS_NEG (

ID_OPPORTUNITE_PUBLIC  int COMMENT 'Numéro de l\'opportunité (correspond à l\'identifiant public dans la source Clinks)'
,IDENTIFIANT_AGENCE  string COMMENT 'code agence'
,CUID_ACTEUR   string  COMMENT 'code alliance de l\'acteur associé à l\'opportunite'
,TYPE_ACTEUR  string COMMENT 'fonction associée à l\'acteur'
,BATCH_NM     string        COMMENT '@string@user ayant soumis l alimentation' 
,CREATION_TS timestamp     COMMENT '@timestamp@timestamp d insertion'
,CURRENT_IN   int           COMMENT '@int@etat courant de la cle'
,FRESH_TS  timestamp     COMMENT '@timestamp@date de fraicheur du fichier source'
,FILE_CD  string        COMMENT '@string@nom du fichier source' 
)

COMMENT 'Table des opportunites '  
PARTITIONED BY (  
               ANNEE                   string        COMMENT 'cle de partionnement',  
               MOIS                    string        COMMENT 'cle de partionnement',  
               DT                      string        COMMENT 'cle de partionnement'				
               )  
STORED AS ORC  
;  

DROP VIEW IF EXISTS ${hiveconf:app_prefix}APP_BPT_VM.VC_OPP_F_OPPORTUNITY_TEAM_MEMBERS_NEG
;
CREATE VIEW IF NOT EXISTS ${hiveconf:app_prefix}APP_BPT_VM.VC_OPP_F_OPPORTUNITY_TEAM_MEMBERS_NEG
COMMENT 'BPT OPPORTUNITE TEAM MEMBER NEGOCE'
AS SELECT
*
FROM
${hiveconf:app_prefix}APP_BPT_VM.T_OPP_F_OPPORTUNITY_TEAM_MEMBERS_NEG
WHERE DT='1900-01-01'
;

-- View   
DROP VIEW IF EXISTS ${hiveconf:app_prefix}APP_BPT_VM_V.V_OPP_F_OPPORTUNITY_TEAM_MEMBERS_NEG;  
CREATE VIEW IF NOT EXISTS ${hiveconf:app_prefix}APP_BPT_VM_V.V_OPP_F_OPPORTUNITY_TEAM_MEMBERS_NEG
COMMENT 'View :OPPORTUNITE TEAM MEMBER NEGOCE'  
AS SELECT  
*  
FROM  
${hiveconf:app_prefix}APP_BPT_VM.T_OPP_F_OPPORTUNITY_TEAM_MEMBERS_NEG
; 

-- View_c   
DROP VIEW IF EXISTS ${hiveconf:app_prefix}APP_BPT_VM_V.V_OPP_F_OPPORTUNITY_TEAM_MEMBERS_NEG_C;  
CREATE VIEW IF NOT EXISTS ${hiveconf:app_prefix}APP_BPT_VM_V.V_OPP_F_OPPORTUNITY_TEAM_MEMBERS_NEG_C
COMMENT 'View :BPT'  
AS SELECT  
*  
FROM  
${hiveconf:app_prefix}APP_BPT_VM.VC_OPP_F_OPPORTUNITY_TEAM_MEMBERS_NEG
; 
