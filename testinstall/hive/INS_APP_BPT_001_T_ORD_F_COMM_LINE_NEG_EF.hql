-- --------------------------------------------------------------------------------------------
-- Creation DDL (tables and views)
-- [GIT_VERSION]
-- Table Ligne Commande Negoce

-- --------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS ${hiveconf:app_prefix}APP_BPT_VM.T_ORD_F_COMM_LINE_NEG_EF;


CREATE EXTERNAL TABLE IF NOT EXISTS ${hiveconf:app_prefix}APP_BPT_VM.T_ORD_F_COMM_LINE_NEG_EF (
NUMERO_DE_LA_LC string COMMENT  'Numéro de la LC - concatenation du capture_id - et line_nbr'
,NUM_DE_COMMANDE  string  COMMENT  'Num. de commande'
,LIB_DOMAINE   string  COMMENT   'Lib Domaine'
,COD_PRODUIT  string  COMMENT  'Cod Produit'
,LIB_PRODUIT  string  COMMENT  'Lib Produit'
,DATE_ENREGISTREMENT_BO   timestamp  COMMENT 'Date Enregistrement BO'
,DATE_DE_MISE_EN_SERVICE  timestamp  COMMENT 'Date de mise en service'
,COD_COMPOSANT   string  COMMENT 'Cod Composant'
,LIB_COMPOSANT   string  COMMENT 'Lib Composant'
,TRIPLETTE   string  COMMENT  'Triplette'
,DATE_SOUHAITEE_DE_LA_LC  timestamp  COMMENT 'Date souhaitée de la LC'
,DATE_DE_VALIDATION_CDE   timestamp  COMMENT 'Date de validation cde'
,LIBELLE_STATUT_LC   string  COMMENT 'Libellé statut LC'
,DATE_DE_RENDEZ_VOUS   timestamp COMMENT 'Date de rendez-vous'
,ETAT_CDE   string  COMMENT  'Etat Cde'
,LIBELLE_TYPE_DOPERATION_PRODUIT   string  COMMENT 'Libellé type dopération produit'
,BATCH_NM     string        COMMENT '@string@user ayant soumis l alimentation' 
,CREATION_TS timestamp     COMMENT '@timestamp@timestamp d insertion'
,CURRENT_IN   int           COMMENT '@int@etat courant de la cle'
,FRESH_TS  timestamp     COMMENT '@timestamp@date de fraicheur du fichier source'
,FILE_CD  string        COMMENT '@string@nom du fichier source' 
)

COMMENT 'Table des Ligne commandes negoce'  
PARTITIONED BY (  
               ANNEE                   string        COMMENT 'cle de partionnement',  
               MOIS                    string        COMMENT 'cle de partionnement',  
               DT                      string        COMMENT 'cle de partionnement'				
               )  
STORED AS ORC  
;  

DROP VIEW IF EXISTS ${hiveconf:app_prefix}APP_BPT_VM.VC_ORD_F_COMM_LINE_NEG_EF
;
CREATE VIEW IF NOT EXISTS ${hiveconf:app_prefix}APP_BPT_VM.VC_ORD_F_COMM_LINE_NEG_EF
COMMENT 'BPT LIGNE COMMANDE NEGOCE'
AS SELECT
*
FROM
${hiveconf:app_prefix}APP_BPT_VM.T_ORD_F_COMM_LINE_NEG_EF
WHERE DT='1900-01-01'
;

-- View   
DROP VIEW IF EXISTS ${hiveconf:app_prefix}APP_BPT_VM_V.V_ORD_F_COMM_LINE_NEG_EF;  
CREATE VIEW IF NOT EXISTS ${hiveconf:app_prefix}APP_BPT_VM_V.V_ORD_F_COMM_LINE_NEG_EF
COMMENT 'View :LIGNE COMMANDE NEGOCE'  
AS SELECT  
*  
FROM  
${hiveconf:app_prefix}APP_BPT_VM.T_ORD_F_COMM_LINE_NEG_EF
; 

-- View_c   
DROP VIEW IF EXISTS ${hiveconf:app_prefix}APP_BPT_VM_V.V_ORD_F_COMM_LINE_NEG_EF_C;  
CREATE VIEW IF NOT EXISTS ${hiveconf:app_prefix}APP_BPT_VM_V.V_ORD_F_COMM_LINE_NEG_EF_C
COMMENT 'View :BPT'  
AS SELECT  
*  
FROM  
${hiveconf:app_prefix}APP_BPT_VM.VC_ORD_F_COMM_LINE_NEG_EF
; 
