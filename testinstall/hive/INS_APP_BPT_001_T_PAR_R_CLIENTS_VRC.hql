-- --------------------------------------------------------------------------------------------
-- Creation DDL (tables and views)
-- ORANGE/DSI E§P
-- S0F0 : LAB : 12/2022
-- @auteur : KJO-- BPT HQL
-- --------------------------------------------------------------------------------------------
USE ${hiveconf:app_prefix}APP_BPT_VM;
DROP TABLE IF EXISTS T_PAR_R_CLIENTS_VRC;
CREATE EXTERNAL TABLE IF NOT EXISTS T_PAR_R_CLIENTS_VRC (
 ETA_Identifiant_RCE               STRING         COMMENT 'Identifiant RCE'
,ETA_Agence_Code                   STRING         COMMENT 'code agence'
,ETA_EDS                           STRING         COMMENT 'EDS niveau etablissement'
,ETA_Raison_Sociale                STRING         COMMENT 'raison sociale établissement'
,ETA_SIRET                         STRING         COMMENT 'siret'
,ETA_Statut_INSEE                  STRING         COMMENT 'statut INSEE de l etablissement'
,ETA_Degre_decision_Code           STRING         COMMENT 'decideur ou non (totalement ou partiellement decideur) (liste decideur / payeur / "installe")'
,ETA_Est_Siege                     STRING         COMMENT 'siege etablissement oui ou non'
,ETA_Num_voie                      INT            COMMENT 'numero de rue'
,ETA_Complement_N_voie            STRING         COMMENT 'complement'
,ETA_Type_voie                     STRING         COMMENT 'type de voie'
,ETA_Nom_voie                      STRING         COMMENT 'nom de la rue'
,ETA_Code_Postal                   STRING         COMMENT 'code postal ville etablissement'
,ETA_Commune                       STRING         COMMENT 'Nom de la ville'
,ETA_Code_NAF                      STRING         COMMENT 'code NAF etablissement'
,ETA_MARCHE_E_IN                   STRING         COMMENT 'marche entreprise  a fait partie du marche entreprise   n appartient pas au marche entreprise'
,ETA_MARCHE_PME_IN                 STRING         COMMENT 'marche PME  a fait partie du marche PME  n appartient pas au marche PME'
,ETA_MARCHE_PRO_IN                 STRING         COMMENT 'marche pro a fait partie du marche PRO  n appartient pas au marche PRO'
,ETA_CUID_VRC                      STRING         COMMENT 'code alliance vendeur responsable de compte niveau etablissement'
,ETA_Nom_Complet_VRC               STRING         COMMENT 'nom et prenom du responsable de compte niveau etablissement'
,ENT_Agence_Code                   STRING         COMMENT 'code de l agence'
,ENT_EDS                           STRING         COMMENT 'EDS niveau entreprise'
,ENT_Identifiant_RCE               STRING         COMMENT 'Identifiant RCE'
,ENT_Raison_Sociale                STRING         COMMENT 'Identifier le client'
,ENT_SIREN                         STRING         COMMENT 'siren lien avec la raison sociale'
,ENT_SIREN_Maison_Mere_ON         STRING         COMMENT 'Indique si l entreprise est Maison Mere ON'
,ENT_Statut_INSEE                  STRING         COMMENT 'statut insee de l entreprise'
,ENT_Seg_Nationale_Code            STRING         COMMENT 'segmentation commerciale de l entreprise'
,ENT_Seg_Locale_Code               STRING         COMMENT 'segmentation commerciale de l entreprise'
,ENT_Scoring                       STRING         COMMENT 'bon ou mauvais payeur',ENT_CUID_VRC                      STRING         COMMENT 'code alliance du vendeur responsable de compte niveau entreprise'
,ENT_Nom_Complet_VRC               STRING         COMMENT 'nom et prenom du responsable de compte niveau entreprise'
,ENT_Code_NAF                      STRING         COMMENT 'code NAF (identification sectorielle d une entreprise (a l INSEE))'
,ENT_MARCHE_E_IN                   STRING         COMMENT 'marche entreprise a fait partie du marche entreprise  n appartient pas au marche entreprise'
,ENT_MARCHE_PME_IN                 STRING         COMMENT 'marche PME a fait partie du marche PME  n appartient pas au marche PME'
,ENT_MARCHE_PRO_IN                 STRING         COMMENT 'marche pro a fait partie du marche PRO  n appartient pas au marche PRO'
,GRP_Agence_Code                   STRING         COMMENT 'code de l agence'
,GRP_EDS                           STRING         COMMENT 'EDS niveau groupe'
,GRP_Identifiant_RCE               STRING         COMMENT 'Identifiant RCE'
,GRP_Raison_Sociale                STRING         COMMENT 'raison sociale du groupe'
,GRP_SIREN_Maison_Mere             STRING         COMMENT 'SIREN groupe maison mere'
,GRP_Seg_Nationale_Code            STRING         COMMENT 'segmentation nationale commerciale du groupe'
,GRP_Seg_Locale_Code               STRING         COMMENT 'segmentation locale commerciale du groupe'
,GRP_CUID_VRC                      STRING         COMMENT 'code alliance vendeur responsable de compte niveau groupe'
,GRP_Nom_Complet_VRC               STRING         COMMENT 'nom du vendeur responsable de compte niveau groupe'
  -- METADATA Fields  
    ,BATCH_NM                    	STRING      		COMMENT  '@STRING@user ayant soumis l alimentation'  
    ,CREATION_TS                  	TIMESTAMP   		COMMENT  '@TIMESTAMP@TIMESTAMP d insertion'  
    ,CURRENT_IN                   	INT         		COMMENT  '@INT@etat courant de la cle'  
    ,FRESH_TS                     	TIMESTAMP   		COMMENT  '@TIMESTAMP@DATE de fraicheur du fichier source'  
    ,FILE_CD                      	STRING      		COMMENT  '@STRING@nom du fichier source' 
)
COMMENT 'Liste des clients'
PARTITIONED BY (
               annee                    STRING          COMMENT 'cle de partionnement annee',
               mois                     STRING          COMMENT 'cle de partionnement mois',
               dt                       STRING          COMMENT 'cle de partionnement dt'

               )
STORED AS ORC
;

-- DDL View Creation

DROP VIEW IF EXISTS ${hiveconf:app_prefix}APP_BPT_VM_V.V_PAR_R_CLIENTS_VRC ;
CREATE VIEW IF NOT EXISTS ${hiveconf:app_prefix}APP_BPT_VM_V.V_PAR_R_CLIENTS_VRC
COMMENT ' : View : Source  : Liste des clients '
AS SELECT
*
FROM
${hiveconf:app_prefix}APP_BPT_VM.T_PAR_R_CLIENTS_VRC
;

-- --------------------------------------------------------------------------------------------
-- SOC ENT
-- Current view : PAR_R_CLIENTS_VRC
-- --------------------------------------------------------------------------------------------

DROP VIEW IF EXISTS ${hiveconf:app_prefix}APP_BPT_VM.VC_PAR_R_CLIENTS_VRC
;
CREATE VIEW IF NOT EXISTS ${hiveconf:app_prefix}APP_BPT_VM.VC_PAR_R_CLIENTS_VRC
AS SELECT
*
FROM
${hiveconf:app_prefix}APP_BPT_VM.T_PAR_R_CLIENTS_VRC
WHERE DT='1900-01-01'
;

-- View
DROP VIEW IF EXISTS ${hiveconf:app_prefix}APP_BPT_VM_V.V_PAR_R_CLIENTS_VRC
;
CREATE VIEW IF NOT EXISTS ${hiveconf:app_prefix}APP_BPT_VM_V.V_PAR_R_CLIENTS_VRC_C
AS SELECT
*
FROM
${hiveconf:app_prefix}APP_BPT_VM.VC_PAR_R_CLIENTS_VRC
;